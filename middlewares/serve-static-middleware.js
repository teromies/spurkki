'use strict';

module.exports = (app) => {
	var express = require('express');
	app.use(express.static('public'));
}