'use strict';

var Promise = require('bluebird');
var jwt = require('jsonwebtoken');
const CONST = require('../config/constants.js');
const ERROR = require('../config/errors.js');

var secret;
module.exports = (app) => {
	secret = app.get('secret');
	app.use(CONST.PROTECTED_ROUTE, authenticate);
}

function authenticate(req, res, next) {

	if (!req.headers[CONST.AUTHORIZATION_HEADER]) return next({status: 400, code: ERROR.AUTHORIZATION_HEADER_MISSING});
	var authorizationSplitted = req.headers[CONST.AUTHORIZATION_HEADER].split(' ');
	if (authorizationSplitted[0] != CONST.AUTHORIZATION_TYPE) return next({status: 400, code: ERROR.AUTHORIZATION_TYPE_ERROR});
	if (authorizationSplitted.length != 2) return next({status: 400, code: ERROR.AUTHORIZATION_TOKEN_MISSING});

	var token = authorizationSplitted[1];

	jwt.verify(token, secret, function(err, decoded) {
		if (err) return next({status: 401, code: ERROR.AUTHENTICATION_FAILURE});    
		req.user = decoded;
		next();
	});
}
