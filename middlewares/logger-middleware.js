'use strict';

const CONST = require('../config/constants.js');

module.exports = (app) => {
	if (!CONST.IS_TEST_ENV) {
		var morgan = require('morgan');
		app.use(morgan('dev'));
	}
}