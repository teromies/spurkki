'use strict';

module.exports = (app) => {
	this.app = app;
}

module.exports.init = () => {
	this.app.use(errorHandler);
}

function errorHandler(err, req, res, next) {
    res.status(err.status || 500).send({
    	success: false,
    	code: err.code ? err.code : 11111
    });
}