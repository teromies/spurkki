'use strict';

const express = require('express');
const path = require('path');
const app = express();

const CONST = require('./config/constants.js');

app.set('secret', CONST.SECRET);

intializeMiddlewares()
	.then(initializeControllers)
	.then(setDefaultResponse)
	.then(connectInteruptSignal)
	.then(initializeErrorHandler)
	.then(startServer);


function initializeControllers() {
	return require('./lib/javascript-loader.js')(__dirname + '/controllers/', app);
}

function intializeMiddlewares() {
	return require('./lib/javascript-loader.js')(__dirname + '/middlewares/', app)
}

function setDefaultResponse() {
	app.use('/', (req, res) => {
		res.status(404).send('Nothing to see here!');
	});
}

function connectInteruptSignal() {
	process.on('SIGINT', () => {
		console.log('\nCaught interrupt signal\n');
	    process.exit();
	});
}

function initializeErrorHandler() {
	return require('./middlewares/error-handler-middleware.js').init();
}

function startServer()  {
	return require('./lib/server')(app);
}

module.exports = app;