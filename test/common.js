var Promise = require('bluebird');
var request = require("supertest-as-promised");
var app = require('../app.js');

module.exports = {
	getToken: getToken
}

function getToken(email, password) {
	return new Promise((resolve, reject) => {
		request(app)
			.post('/login')
			//.set('Content-type', 'application/json')
		    .send({email: email, password: password})
			.end((err, res) => {
				if (err) reject(err);
				resolve(res.body.token);
			});
	});
}