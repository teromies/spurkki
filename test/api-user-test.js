var request = require("supertest-as-promised");
var expect = require('chai').expect;

var app = require('../app.js');
var User = require('../models/user-model.js');
const CONST = require('../config/constants.js');
const ERROR = require('../config/errors.js');
var common = require('./common.js');

var TEST_USER = {
	email: 'test_username',
	password: '12345678'
};

/*********************************************************************************************/
/* USER API - implemented in user-controller.js
/*********************************************************************************************/
describe('User API', () => {

	after(() => {
		return User.findOneAndRemove({ email: TEST_USER.email }).exec();
	});

	describe('CREATE', () => {	
		it('should return correct error when email is missing', () => {
			return request(app)
				.post('/user')
				.send({password: TEST_USER.password})
				.expect(400, {
					success: false,
					code: ERROR.EMAIL_MISSING
				});
		});

		it('should return correct error when password is missing', () => {
			return request(app)
				.post('/user')
				.send({email: TEST_USER.email})
				.expect(400, {
					success: false,
					code: ERROR.PASSWORD_MISSING
				});
		});

		it('should create an user successfully', () => {
			return request(app)
				.post('/user')
				.send({email: TEST_USER.email, password: TEST_USER.password})
				.expect(200)
				.then(res => {
					expect(res.body._id).to.be.not.empty;
					TEST_USER._id = res.body._id;
				});
		});

		it('should return correct error when username already exists', () => {
			return request(app)
				.post('/user')
				.send({email: TEST_USER.email, password: TEST_USER.password})
				.expect(500, {
					success: false,
					code: ERROR.USER_ALREADY_EXISTS
				});
		});
	});

	describe('READ', () => {
		before(() => {
			return common.getToken(TEST_USER.email, TEST_USER.password)
				.then((res) => {
					TOKEN = res;
				});
		});

		it('should get users successfully', () => {
			return request(app)
				.get(CONST.USER_ROUTE)
				.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.expect(200)
				.then(res => {
					expect(res.body[0].email).to.equal(TEST_USER.email);
					expect(res.body[0].password).to.be.empty;
					expect(res.body[0]._id).to.equal(TEST_USER._id.toString());
				});
		});

		xit('should get user by id successfully', () => {
			return request(app)
				.get(CONST.USER_ROUTE + '/' + TEST_USER._id)
				.set(CONST.AUTHORIZATION_HEADER,  CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.expect(200, {
					email: TEST_USER.email,
					_id: TEST_USER._id.toString()
				});
		});

		it('should get user profile successfully', () => {
			return request(app)
				.get(CONST.USER_ROUTE + '/me')
				.set(CONST.AUTHORIZATION_HEADER,  CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.expect(200, {
					email: TEST_USER.email,
					_id: TEST_USER._id
				});
		});
	});

	describe('UPDATE', () => {	

		it('should return correct error when gender is missing', () => {
			return request(app)
				.put(CONST.USER_ROUTE)
				.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.expect(400, {
					success: false,
					code: ERROR.GENDER_MISSING
				});
		});

		it('should return correct error when birthdate is missing', () => {
			return request(app)
				.put(CONST.USER_ROUTE)
				.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.send({gender: CONST.GENDER.male})
				.expect(400, {
					success: false,
					code: ERROR.BIRTHDATE_MISSING
				});
		});

		it('should return correct error when birthdate is missing', () => {
			return request(app)
				.put(CONST.USER_ROUTE)
				.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.send({gender: CONST.GENDER.male, birthdate: new Date()})
				.expect(400, {
					success: false,
					code: ERROR.HEIGHT_MISSING
				});
		});

		xit('should return correct error when user is not found', () => {
			return request(app)
				.put(CONST.USER_ROUTE + '/aaaaaaaaaaaaaaaaa')
				.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.send({gender: CONST.GENDER.male, birthdate: new Date(), height: 177})
				.expect(500, {
					success: false,
					code: ERROR.USER_NOT_FOUND
				})
		});

		it('should modify an user by id successfully',() => {
			var date = new Date();
			var modifiedUser = {
				_id: TEST_USER._id,
				gender: CONST.GENDER.female,
				birthdate: date,
				height: 166
			}

			return request(app)
				.put(CONST.USER_ROUTE)
				.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.send(modifiedUser)
				.expect(200, {
					_id:  modifiedUser._id,
					birthdate: modifiedUser.birthdate.toISOString(),
					email: TEST_USER.email,
					gender: modifiedUser.gender,
					height: modifiedUser.height
				});
		});
	});

	describe('DELETE', () => {	
		it('should delete user by id successfully', () => {
			return request(app)
				.delete(CONST.USER_ROUTE)
				.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
				.expect(200, {
					_id: TEST_USER._id.toString(),
				});
		});
	});

});