'use strict';

var request = require("supertest-as-promised");

var app = require('../app.js');
const CONST = require('../config/constants.js');
const ERROR = require('../config/errors.js');
var common = require('./common.js');
var User = require('../models/user-model.js');

/*********************************************************************************************/
/* AUTHENTICATION - implemented in authentication-middleware.js
/*********************************************************************************************/
describe('Authentication', () => {
	
	it('should not require authentication in route /', () => {
		return request(app)
			.get('/')
			.expect(200);
	});

	it('should return the right error when authorization header is missing', () => {
		return request(app)
			.post(CONST.PROTECTED_ROUTE)
			.expect(400, {
				success: false,
				code: ERROR.AUTHORIZATION_HEADER_MISSING
			});
	});

	it('should return the right error when authorization type is not correct', () => {
		return request(app)
			.post(CONST.PROTECTED_ROUTE)
			.set(CONST.AUTHORIZATION_HEADER, 'Type test')
			.expect(400, {
				success: false,
				code: ERROR.AUTHORIZATION_TYPE_ERROR
			});
	});

	it('should return the right error when authorization token is missing', () => {
		return request(app)
			.post(CONST.PROTECTED_ROUTE)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE)
			.expect(400, {
				success: false,
				code: ERROR.AUTHORIZATION_TOKEN_MISSING
			});
	});

	it('should return the right error when the token is wrong', () => {
		return request(app)
			.post(CONST.PROTECTED_ROUTE)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' test')
			.expect(401, {
					success: false,
					code: ERROR.AUTHENTICATION_FAILURE
			});
	});

	xit('should return the right response when authentication is successful', () => {
		var TEST_USER = {
		  	email: 'test@test.fi',
	  		password: 'abc'
		}
		var user = User({
		  	email: TEST_USER.email,
	  		password: TEST_USER.password
		});

		return user.save()
			.then(created => {
				TEST_USER = created;
				return common.getToken(TEST_USER.email, TEST_USER.password);
			})
			.then(token => {
				return request(app)
					.post(CONST.PROTECTED_ROUTE)
					.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + token)
					.expect(404);
				})
			.then(() => {
				return User.findByIdAndRemove(TEST_USER._id).exec();
			});
	});
});
