var request = require("supertest-as-promised");
var expect = require('chai').expect;
var ObjectID = require('mongodb').ObjectID;

var app = require('../app.js');
const CONST = require('../config/constants.js');
const ERROR = require('../config/errors.js');
var common = require('./common.js');
var User = require('../models/user-model.js');

var TOKEN;	

var TEST_USER = {
	email: "test@test.fi",
	password: "abc"
}

var TEST_MEASUREMENT = {
	date: Date.now(),
	date_hour: 10,
	weight: 10,
	bmi: 10,
	fat_percentage: 10,
	muscle_percentage: 10,
	resting_metabolism: 10,
	visceral_fat: 10
}

/*********************************************************************************************/
/* MEASUREMENT API - implemented in measurement-controller.js
/*********************************************************************************************/
describe('Measurement API', () => {

	before(() => {
		var user = User({
			email: TEST_USER.email,
			password: TEST_USER.password
		});
		return user.save()
			.then(created => {
				return common.getToken(TEST_USER.email, TEST_USER.password);
			})
			.then(res => {
				TOKEN = res;
		});
	});

	after(() => {
		return User.findOneAndRemove({email: TEST_USER.email}).exec();
	})

	it('should return the right error message when date is missing', () => {
		return request(app)
			.post(CONST.MEASUREMENT_ROUTE)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
			.send({user_id: 'test'})
			.expect(400, {
				success: false,
				code: ERROR.DATE_MISSING
			});	
	});

	it('should return the right error message when weight is missing', () => {
		return request(app)
			.post(CONST.MEASUREMENT_ROUTE)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
			.send({user_id: 'test', date: new Date()})
			.expect(400, {
				success: false,
				code: ERROR.WEIGHT_MISSING
			})
	});

	it('should create one measurement successfully', () => {
		return request(app)
			.post(CONST.MEASUREMENT_ROUTE)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
			.send(TEST_MEASUREMENT)
			.expect(200)
			.then(res => {
				expect(res.body.weight).to.equal(TEST_MEASUREMENT.weight);
				TEST_MEASUREMENT._id = ObjectID(res.body._id);
			});
	});	

	it('should get number of users measurements', () => {
		return request(app)
			.get(CONST.MEASUREMENT_ROUTE)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
			.expect(200)
			.then(res => {
				expect(res.body.count).to.equal(1);
			});
	});
	
	it('should modify measurement successfully', () => {
		TEST_MEASUREMENT.weight = 20;
		return request(app)
			.put(CONST.MEASUREMENT_ROUTE + '/' + TEST_MEASUREMENT._id)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
			.send(TEST_MEASUREMENT)
			.expect(200)
			.then(res => {
				expect(res.body.weight).to.equal(TEST_MEASUREMENT.weight);
			});
	});
	
	it('should delete measurement by id successfully', () => {
		return request(app)
			.delete(CONST.MEASUREMENT_ROUTE + '/' + TEST_MEASUREMENT._id)
			.set(CONST.AUTHORIZATION_HEADER, CONST.AUTHORIZATION_TYPE + ' ' + TOKEN)
			.expect(200)
			.then(res => {
				expect(res.body._id).to.equal(TEST_MEASUREMENT._id.toString());
			});
	});
});