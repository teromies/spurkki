
var assert = require('chai').assert;
var expect = require('chai').expect;
var User = require('../models/user-model.js');

describe('User model', function() {

	const TEST_EMAIL = 'testemail';

	after(() => {
		return User.findOneAndRemove({ email: TEST_EMAIL }).exec();
	});

	it('should create an user successfully', () => {
		var PASSWORD = 'abc123';
		var newUser = new User({
			email: TEST_EMAIL,
			password: PASSWORD
		});
		return newUser.save()
			.then(function(res) {
				assert.equal(res.email, TEST_EMAIL, 'email did not match');
				expect(res.password).to.be.not.empty;
			});
	});

	it('should get users successfully', () => {
		return User.find()
			.then(function(res) {
				assert.equal(res[0].email, TEST_EMAIL, 'username did not match');
			})
	});

	it('should get the user successfully by id', () => {
		return User.find()
			.then(function(res) {
				return res[0]._id;
			})
			.then(function(res) {
				return User.findById(res);
			})
			.then(function(res) {
				assert.equal(res.email, TEST_EMAIL, 'email did not match');
			});
	});

	it('should delete the user successfully', () => {
		return User.find()
			.then(function(res) {
				return res[0]._id;
			})
			.then(function(res) {
				return User.findByIdAndRemove(res);
			})
			.then(function() {
				return User.find();
			})
			.then(function(res) {
				assert.notOk(res.length, 'response was not emtpy');
			})
	});
});