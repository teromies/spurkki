'use strict';

const SERVER_HOST = 'localhost';
const SERVER_PORT = 3000;
const SECRET = 'testisecretti';
const AUTHORIZATION_HEADER = 'authorization';
const AUTHORIZATION_TYPE = 'Bearer';

const GENDER = {
	'female': 1,
	'male': 2
}

// ROUTES
const PROTECTED_ROUTE = '/omron';
const USER_ROUTE = PROTECTED_ROUTE + '/user';
const MEASUREMENT_ROUTE = PROTECTED_ROUTE + '/measurement';
const LOGIN_ROUTE = '/login';

var IS_TEST_ENV = process.env.NODE_ENV == 'test';
var MONGO_ADDRESS = IS_TEST_ENV ? 'mongodb://localhost/omron_test' : 'mongodb://localhost/omron';

module.exports = {
	SERVER_HOST: SERVER_HOST,
	SERVER_PORT: SERVER_PORT,
	SECRET: SECRET,
	AUTHORIZATION_HEADER: AUTHORIZATION_HEADER,
	AUTHORIZATION_TYPE: AUTHORIZATION_TYPE,
	GENDER: GENDER,
	MONGO_ADDRESS: MONGO_ADDRESS,
	PROTECTED_ROUTE: PROTECTED_ROUTE,
	USER_ROUTE: USER_ROUTE,
	MEASUREMENT_ROUTE: MEASUREMENT_ROUTE,
	LOGIN_ROUTE: LOGIN_ROUTE,
	IS_TEST_ENV: IS_TEST_ENV
}