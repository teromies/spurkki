'use strict';

var bcrypt = require('bcryptjs');
var mongoose = require('../lib/database.js');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	email: {type: String, required: true, unique: true, lowercase: true},
	password: {type: String, required: true, select: false},
	username: String,
	gender: Number, // 1 = female, 2 = male
	birthdate: Date,
	height: Number,
	created_at: Date,
	updated_at: Date
}, {versionKey: false});

userSchema.pre('save', function(next) {
    var user = this;

    if (!user.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(user.password, salt, function(err, hash) {
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(password, done) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        done(err, isMatch);
    });
};

module.exports = mongoose.model('User', userSchema);