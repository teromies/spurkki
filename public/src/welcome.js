import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';
import {Router} from 'aurelia-router';

@inject(AuthService, Router)
export class Welcome {

    constructor(auth, router) {
        this.auth = auth;
        this.router = router;
    }

    activate() {
    	if (this.auth.isAuthenticated()) {
    		this.router.navigate('measurement');
    	}
    }
}