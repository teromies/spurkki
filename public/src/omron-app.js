import {I18N} from 'aurelia-i18n';
import XHR from 'i18next-xhr-backend';
import config from './auth-config';

export function configure(aurelia) {
	aurelia.use
		.standardConfiguration()
		.developmentLogging()
		.plugin('aurelia-auth', (baseConfig) => {
			baseConfig.configure(config);
		})
		.plugin('aurelia-i18n', (instance) => {
			// register backend plugin
			instance.i18next.use(XHR);

			// adapt options to your needs (see http://i18next.com/docs/options/)
			instance.setup({
			 backend: {                                  
			    loadPath: '/locales/{{lng}}/{{ns}}.json',
			 },
					
			 lng : 'fi',
			 attributes : ['t','i18n'],
			 fallbackLng : 'en',
			 debug : false
			});
		});

	aurelia.start().then(a => a.setRoot());
}