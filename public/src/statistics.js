import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import {AuthService} from 'aurelia-auth';
import {Router} from 'aurelia-router';
import moment from 'moment';

@inject(HttpClient, AuthService, Router)

export class Statistics {
    constructor(http, auth, router) {
        this.http = http;
        this.auth = auth;
        this.router = router;
        this.success = false;
        this.measurements = [];
        this.modifying = null;
        this.refreshing = false;
        this.averages = {
            weight: 0,
            bmi: 0,
            fat_percentage: 0,
            muscle_percentage: 0,
            resting_metabolism: 0,
            visceral_fat: 0
        };
    }

    activate(){
        this.fetchAllMeasurements();
    }

    fetchAllMeasurements() {
        this.refreshing = true;
        return this.http.fetch('/omron/measurement/all', {
            method: 'get'
        })
        .then(response => response.json())
        .then(meas => {
            this.measurements = meas;

            this.calculateAverages(meas);

            this.refreshing = false;
        });
    }

    calculateAverages(meas) {
        meas.forEach(m => {
            this.averages.weight += m.weight;
            this.averages.bmi += m.bmi;
            this.averages.fat_percentage += m.fat_percentage;
            this.averages.muscle_percentage += m.muscle_percentage;
            this.averages.resting_metabolism += m.resting_metabolism;
            this.averages.visceral_fat += m.visceral_fat;
        });
         for (let key in this.averages) {
            this.averages[key] /= meas.length;
        }
    }

    exportMeasurements() {
        let csv = "Päivämäärä;Aika;Massa;Painoindeksi;Rasvaprosentti;Lihasprosentti;Lepoaineenvaihdunta;Sisäelirasva\n";

        this.measurements.forEach(m => {
            csv += moment(m.date).format('DD.MM.YYYY') + ';'+  moment(m.date).format('HH:mm') + ';' + m.weight + ';' + m.bmi + ';' + 
                   m.fat_percentage + ';' + m.muscle_percentage + ';' +  m.resting_metabolism + ';' + m.visceral_fat + '\n';
        });

        var pom = document.createElement('a');
        pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(csv));
        var filename = 'measurements-' + moment(new Date()).format('YYYY-MM-DD') + '.csv';
        pom.setAttribute('download', filename);
        pom.style.display = 'none';
        document.body.appendChild(pom);
        pom.click();
        document.body.removeChild(pom);
    }

    modify(mea) {
        this.modifying = mea;
    }

    save(mea) {
        return this.http.fetch('/omron/measurement/' + mea._id, {
            method: 'put',
            body: json(mea)
        })
        .then(this.modifying = null);
    }

    remove(id) {
        if (confirm('Haluatko varmasti poistaa mittauksen?')) {
            return this.http.fetch('/omron/measurement/' + id, {
                method: 'delete'
            })
            .then(this.fetchAllMeasurements.bind(this));
        }
    }
}