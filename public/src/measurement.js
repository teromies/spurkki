import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import {AuthService} from 'aurelia-auth';
import {Router} from 'aurelia-router';
import 'jquery';

@inject(HttpClient, AuthService, Router)
export class Measurement {

    constructor(http, auth, router) {
        this.http = http;
        this.auth = auth;
        this.router = router;
        this.success = false;
        this.measurement = {};
    }

    activate(){
        this.fetchPreviousMeasurement();
    }

    save() {

    	if (!this.auth.isAuthenticated()) {
    		this.router.navigate('login');
    	}
        this.success = false;

            return this.http.fetch('/omron/measurement', {
                method: 'post',
                body: json(this.measurement)
            })
                .then(response => response.json())
                .then(data => {
                    this.success = true;
                    console.log("Measurement saved");

                    setTimeout(function () {
                        $('.alert-success').remove();
                    }, 3000);
                });
    }

    fetchPreviousMeasurement() {
        return this.http.fetch('/omron/measurement/1', {
            method: 'get'
            })
            .then(response => response.json())
            .then(mea => this.measurement = mea[0]);
    }
}