import {AuthService} from 'aurelia-auth';
import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import moment from 'moment';

@inject(AuthService, HttpClient) 
export class Profile {
  
	constructor(auth, http) {
		this.auth = auth;
		this.http = http;
		this.profile = null;
		this.error = '';
		this.success = false;
		this.genders = ['female', 'male'];
		this.selectedGender = null;
		// Age needs to be between 6 and 80
		this.maxBirthdate = moment(new Date()).subtract(6, 'years').format('YYYY-MM-DD');
		this.minBirthdate = moment(new Date()).subtract(80, 'years').format('YYYY-MM-DD');
	}

	activate() {
		return this.auth.getMe()
			.then(data => {
				this.selectedGender = this.genders[data.gender - 1];
				this.profile = data;
			});
	}

	update() {
		this.success = false;
		this.error = '';
		this.profile.gender = this.genders.indexOf(this.selectedGender) + 1;
		return this.http.fetch('/omron/user', {
			method: 'put',
			body: json(this.profile)
		})
		.then(response => response.json())
		.then(data => {
			if (data.code){
				this.error = data.code;
			} else {
				this.success = true;
			}
		});	
	}

}