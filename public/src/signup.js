import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';

// Using Aurelia's dependency injection, we inject the AuthService
// with the @inject decorator
@inject(AuthService)

export class Signup {
    email = '';
    password = '';

    signupError = '';

      constructor(auth) {
        this.auth = auth;
      };

    signup() {

        var userInfo = { email: this.email, password: this.password }

        return this.auth.signup(userInfo)
            .then(response => {
              console.log(response)
              console.log("Signed Up!");
            })
            .catch(error => error.json().then(serverError => { 
                this.signupError = serverError.code;
            }));
      };
}