import {AuthorizeStep} from 'aurelia-auth';
import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';


@inject(Router)
export default class {

  constructor(router) {
    this.router = router;
  };

  configure() {

    var appRouterConfig = function(config) {

      config.addPipelineStep('authorize', AuthorizeStep);

      // TODO: Lokalisointi puuuttuu otsikoista
      config.title = '';
      config.map([
          { route: ['','welcome'], moduleId: './welcome', nav: false, title: 'Etusivu' },
          { route: 'signup', moduleId: './signup', nav: false, title: 'Signup'},
          { route: 'login', moduleId: './login', nav: false, title: 'Login' },
          { route: 'profile', moduleId: './profile', nav: false, title: 'Profile', auth: true},
          { route: 'measurement', moduleId: './measurement', nav: true, title: 'Lisää mittaus', auth: true},
          { route: 'statistics', moduleId: './statistics', nav: true, title: 'Mittaustulokset', auth: true},
          { route: 'graphs', moduleId: './graphs', nav: true, title: 'Kuvaajat', auth: true}
        ]);
      };
    this.router.configure(appRouterConfig);
  };
}
