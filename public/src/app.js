import 'bootstrap';
//import 'bootstrap/css/bootstrap.css!';

import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
//import HttpClientConfig from 'aurelia-auth/app.httpClient.config';
import {FetchConfig} from 'aurelia-auth';
import AppRouterConfig from 'router-config';
import {I18N} from 'aurelia-i18n';

// Using Aurelia's dependency injection, we inject Aurelia's router,
// the aurelia-auth http client config, and our own router config
// with the @inject decorator.
@inject(Router, FetchConfig, AppRouterConfig, I18N)

export class App { 
  constructor(router, fetchConfig, appRouterConfig, i18n) {
    this.router = router;
    this.fetchConfig = fetchConfig;
    this.appRouterConfig = appRouterConfig;
    this.i18n = i18n;
    this.i18n.setLocale(window.localStorage.locale || 'fi');
  };

  activate() {

    // Here, we run the configuration when the app loads.
    this.fetchConfig.configure();
    this.appRouterConfig.configure();

  };
}