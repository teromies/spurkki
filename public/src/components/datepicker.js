import { customElement, bindable, inject } from 'aurelia-framework';

import 'jquery';
import { datepicker } from 'jquery-ui';

@customElement('datepicker')
@inject(Element)
export class JqueryUiDatepicker {
    @bindable id = '';
    @bindable name = '';
    @bindable options = {};

    constructor(Element) {
        this.element = Element;

        if (!this.id && this.name) {
            this.id = this.name;
        }

        if (!this.name && this.id) {
            this.name = this.id;
        }
    }

    attached() {
        var options = {};
        if (!$.isEmptyObject(this.options)) {
            options = JSON.parse(this.options);
        }

        $(this.element).find('input').datepicker(options)
            .on('change', e => {
                let changeEvent = new CustomEvent('input', {
                    detail: {
                        value: e.val
                    },
                    bubbles: true
                });

                this.element.dispatchEvent(changeEvent);
            });
    }

    detached() {
        //$(`#${this.id}`).datepicker('destroy').off('change');
    }
}