// client/src/auth-config.js

// Specific settings for our application's
// authentication context. These will override
// the default settings provided by aureliauth

var config = {

  // Our Node API is being served from localhost:3001
  baseUrl: 'http://localhost:3000',
  // The API specifies that new users register at the POST /users enpoint.
  signupUrl: 'user',
  // Logins happen at the POST /sessions/create endpoint.
  loginUrl: 'login',
  // The API serves its tokens with a key of id_token which differs from
  // aureliauth's standard.
  tokenName: 'token',
  // Once logged in, we want to redirect the user to the welcome view.
  loginRedirect: '#/measurement',
  logoutRedirect: '#/',

  signupRedirect: '#/login',
  profileUrl: '/omron/user/me'

}

export default config;