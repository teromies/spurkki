import {AuthService} from 'aurelia-auth';
import {inject} from 'aurelia-framework';
@inject(AuthService )

export class Login{

	constructor(auth) {
		this.auth = auth;
		this.error = '';
	};

	email = '';
	password = '';

	login() {
		this.error = '';
	    var creds = "grant_type=password&email=" + this.email + "&password=" + this.password;
		return this.auth.login(this.email, this.password)
			.catch(err => err.json())
			.then(err => {
				if (err) {
					this.error = err.code;
				}
			});
	};
	
	authenticate(name) {
		return this.auth.authenticate(name, false, null)
			.then((response) =  {
		});
	}

	forgotPassword() {
		alert('Not implemented yet!');
	}
}